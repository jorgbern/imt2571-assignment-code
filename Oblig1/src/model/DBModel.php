<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
          $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'root', '');
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		try {				// making a try catch block
		$bookArray = $this->db->query('SELECT * FROM book'); // retrieving books
		//if(is_array($bookArray) || is_object($bookArray)) {
			foreach($bookArray as $book) {						// and running through the table.
				$booklist[] = new Book(							// Making a new book object
							$book['title'],					// with title, author..
							$book['author'],
							$book['description'],
							$book['id']
							);
			}
			
		} catch(PDOException $ex) {			// throwing a PDO exception
			$view = new ErrorView();		// displaying an error when they occure.
			$view->create();
		}
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		try {
			$tempBook = $this-> db-> prepare("SELECT * FROM book WHERE id=?");
			$tempBook->bindValue(1, $id, PDO::PARAM_INT);	// prepared statement
			$tempBook->execute();				// retrieving book and cheking valid id
			$tempRow = $tempBook->fetch(PDO::FETCH_ASSOC); // fetch the book
			
			if($tempRow != '') {			// if the imported book excist
				$book = new Book (			// make a new book object
							$tempRow['title'],	// with title, author...
							$tempRow['author'],
							$tempRow['description'],
							$tempRow['id']
							);
			} else {
				return null;
			}
			
		} catch (PDOException $ex) {
			$view = new ErrorView();
			$view->create();
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if ($book->title != "" AND $book->author != ""){ //checking that input is valid
			if($book->description == "")			// setting desc. = null if empty
				$book->description == null;
			try {
				$tempBook = $this-> db-> prepare("INSERT INTO book (title, author, description) VALUES (?, ?, ?)");
				$tempBook->execute(array($book->title, $book->author, $book->description));
				$book->id = $this-> db-> lastInsertId(); // validating id as the last 
													// inserted in the database
			} catch(PDOException $ex) {
				$view = new ErrorView();
				$view->create();
			}
		}
		else {
			$view = new ErrorView(); // if the input is not valid show an error
			$view->create();
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if ($book->title != "" OR $book->author != ""){ // checking input is valid
			if($book->description == "")				// desc=null if empty
				$book->description == null;
		try {
		$updBook = $this-> db-> prepare("UPDATE book SET title=?, 
														author=?, 
														description=? 
														WHERE id=?");
		$updBook->execute(array($book->title, 	//updating title, author..
								$book->author, 
								$book->description, 
								$book->id));
		} catch(PDOException $ex) {
			$view = new ErrorView();
			$view->create();
		}
    } else {
			$view = new ErrorView();
			$view->create();
	}
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
		$delBook = $this-> db-> prepare("DELETE FROM book WHERE id=?");
		$delBook->execute(array($id));
		} catch(PDOException $ex) {
			$view = new ErrorView();
			$view->create();
		}
    }
	
}

?>